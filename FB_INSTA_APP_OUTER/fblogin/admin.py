from django.contrib import admin
from .models import DealerType, SysUser, Sales, Smartcards, Region, District, Dealer_ESP, DealerVisitData
from import_export.admin import ExportActionMixin, ExportActionModelAdmin
from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget
from rangefilter.filter import DateRangeFilter


class DealerTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name']

class DealerESPResource(resources.ModelResource):
    dealer_id = fields.Field(
        column_name='dealer',
        attribute='dealer',
        widget=ForeignKeyWidget(SysUser, 'user'))

    dealerAssociate_id = fields.Field(
        column_name='dealerAssociate',
        attribute='dealerAssociate',
        widget=ForeignKeyWidget(SysUser, 'user'))

    class Meta:
        fields = ('dealerAssociate_id', 'dealer_id','created_date')
        model = Dealer_ESP

class DealerESPAdmin(ExportActionModelAdmin):
    list_display = ('id', 'dealer', 'dealerAssociate', 'created_date')
    list_filter = (('created_date', DateRangeFilter), ('updated_date', DateRangeFilter), 'created_date', 'updated_date')
    search_fields = ['dealerAssociate__user__username', 'dealer__user__username']
    resource_class = DealerESPResource


class SysUserAdmin(ExportActionModelAdmin):
    list_display = ('id', 'user', 'type', 'phoneNumber', 'created_date', 'updated_date')
    search_fields = ['user__username', 'type__name', 'phoneNumber']


class RegionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name']


class DistrictAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'region')
    search_fields = ['name', 'region__name']


class SalesResource(resources.ModelResource):
    dealer_id = fields.Field(
        column_name='dealer',
        attribute='dealer',
        widget=ForeignKeyWidget(SysUser, 'user'))

    class Meta:
        model = Sales


class SalesAdmin(ExportActionModelAdmin):
    resource_class = SalesResource
    list_display = ('sold', 'dealer', 'created_date', 'updated_date')
    search_fields = ['dealer__user__username', 'sold']
    list_filter = (('created_date', DateRangeFilter), ('updated_date', DateRangeFilter))
    date_hierarchy = 'created_date'


class SmartcardsResource(resources.ModelResource):
    dealer_id = fields.Field(
        column_name='dealer',
        attribute='dealer',
        widget=ForeignKeyWidget(SysUser, 'user'))

    class Meta:
        model = Smartcards


class SmartcardsAdmin(ExportActionModelAdmin):
    list_filter = (('created_date', DateRangeFilter), ('updated_date', DateRangeFilter), 'created_date', 'updated_date')
    date_hierarchy = 'created_date'
    list_display = ('id', 'smartcard_number', 'dealer', 'created_date', 'updated_date')
    search_fields = ['smartcard_number', 'dealer__user__username']
    resource_class = SmartcardsResource

class DealerVisitDataResource(resources.ModelResource):
    dealer_id = fields.Field(
        column_name='dealer',
        attribute='dealer',
        widget=ForeignKeyWidget(SysUser, 'user'))

    class Meta:
        model = Smartcards

class DealerVisitDataAdmin(ExportActionModelAdmin):
    list_filter = (('created_date', DateRangeFilter), ('updated_date', DateRangeFilter), 'created_date', 'updated_date')
    date_hierarchy = 'created_date'
    list_display = ('dealer', 'StockAvailable', 'StockPush', 'StockBalance', 'created_date', 'updated_date')
    search_fields = ['dealer__user__username']
    resource_class = DealerVisitDataResource


admin.site.register(SysUser, SysUserAdmin)
admin.site.register(DealerVisitData, DealerVisitDataAdmin)
admin.site.register(DealerType, DealerTypeAdmin)
admin.site.register(Sales, SalesAdmin)
admin.site.register(Smartcards, SmartcardsAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(District, DistrictAdmin)
admin.site.register(Dealer_ESP, DealerESPAdmin)

