# Generated by Django 3.2.6 on 2021-08-29 15:11

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('fblogin', '0014_dealervisitdata'),
    ]

    operations = [
        migrations.AddField(
            model_name='dealervisitdata',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='dealervisitdata',
            name='updated_date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
