# Generated by Django 3.1.3 on 2021-01-23 20:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fblogin', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DealerType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
            ],
        ),
    ]
