from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now


class DealerType(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Region(models.Model):
    name = models.CharField(max_length=50, help_text='Region')

    def __str__(self):
        return self.name


def get_region():
    return Region.objects.get_or_create(id=1)[0].id


class District(models.Model):
    name = models.CharField(max_length=50, help_text='District:')
    region = models.ForeignKey(Region, on_delete=models.CASCADE, default=get_region)

    def __str__(self):
        return self.name


def get_district():
    return District.objects.get_or_create(id=1)[0].id


class SysUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    type = models.ForeignKey(DealerType, on_delete=models.CASCADE, default=1)
    region = models.ForeignKey(Region, on_delete=models.CASCADE, default=get_region)
    district = models.ForeignKey(District, on_delete=models.CASCADE, default=get_district)
    phoneNumber = models.CharField(max_length=10, default='0714123123')
    created_date = models.DateTimeField(default=now)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username


class Sales(models.Model):
    sold = models.IntegerField(help_text='How many did you sales today?')
    dealer = models.ForeignKey(SysUser, on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=now)
    updated_date = models.DateTimeField(auto_now=True)
    sold_date = models.DateTimeField(default=now)

    def __str__(self):
        return self.dealer.user.username+' -  '+str(self.sold)


class Smartcards(models.Model):
    smartcard_number = models.CharField(max_length=30)
    dealer = models.ForeignKey(SysUser, on_delete=models.CASCADE)
    created_date = models.DateTimeField()
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.dealer.user.username + ' -  ' + self.smartcard_number

def get_megadealer():
    d_type = None
    user_obj = None
    re_obj = None
    try:
        return User.objects.get(id=1)
    except:
        user_obj = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        try:
            d_type=DealerType.objects.get(id=1)
        except:
            d_type = DealerType(name='Mega-Dealer')

        try:
            re_obj=Region.objects.get(id=1)
        except:
            re_obj = Region(name='Arusha')

        sysuser = SysUser(User=user_obj, type=d_type, region=re_obj)

class MegaDealer_Subdealer(models.Model):
    esp = models.ForeignKey(SysUser, on_delete=models.CASCADE, related_name='esp')
    megadealer = models.ForeignKey(SysUser, on_delete=models.CASCADE, default=get_megadealer)
    created_date = models.DateTimeField(default=now)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.esp.user.username+' -  '+self.megadealer.user.username

class Dealer_ESP(models.Model):
    dealer = models.ForeignKey(SysUser, on_delete=models.CASCADE, related_name='dealer')
    dealerAssociate = models.ForeignKey(SysUser, on_delete=models.CASCADE, default=get_megadealer)
    created_date = models.DateTimeField(default=now)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.dealer.user.username+' -  '+self.dealerAssociate.user.username

class DealerVisitData(models.Model):
    dealer = models.ForeignKey(Dealer_ESP, on_delete=models.CASCADE, related_name='dealer_visit', default=Dealer_ESP.objects.get(pk=1).pk)
    StockAvailable = models.IntegerField(help_text='What is the stock available?')
    StockPush = models.IntegerField(help_text='How many decoders did you push?')
    StockBalance = models.IntegerField()
    VisitType = models.CharField(max_length=30, default='Mega-Dealer')
    created_date = models.DateTimeField(default=now)
    updated_date = models.DateTimeField(auto_now=True)

