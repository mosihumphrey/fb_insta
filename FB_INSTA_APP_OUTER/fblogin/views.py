from django.views.generic import TemplateView, FormView
from .forms import RegisterForm, LoginForm, SalesForm, MegaDealerForm, ChangePasswordForm, ChangePhonenumberForm,\
    RegisterDealerForm, DealerVisitForm
from .models import SysUser, DealerType, Sales, Smartcards, Region, District, MegaDealer_Subdealer, Dealer_ESP, DealerVisitData
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.utils.timezone import now
from django.core.exceptions import ObjectDoesNotExist
import datetime

def logoutView(request):
    logout(request)
    return redirect('loginPage')


"""
def changePassword(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = ChangePasswordForm(request.user)
    return render(request, 'phonenumber.html', {
        'form': form
    })
"""

class ChangePhonenumberView(FormView):
    template_name = "changephonenumber.html"
    form_class = ChangePhonenumberForm
    success_url = None

    def get(self, request):
        if request.user.is_authenticated:
            return super(FormView, self).get(request)
        else:
            messages.error(self.request,
                           'Login kwanza ndipo utaweza kubadili namba yako ya simu')
            return redirect('loginPage')

    # Adding current phone number
    def get_context_data(self, **kwargs):
        context = super(ChangePhonenumberView, self).get_context_data(**kwargs)
        dealer = SysUser.objects.get(user=self.request.user)
        context['phoneNumber'] = dealer.phoneNumber
        return context

    def form_valid(self, form):
        #Data is now clean please submit to DB authentication first check again if user is logged in
        if self.request.user.is_authenticated:
            new_phonenumber = form.cleaned_data['phoneNumber']

            #Try to update phone number
            try:
                dealer = SysUser.objects.get(user=self.request.user)
                dealer.phoneNumber = new_phonenumber
                dealer.save() #Save the data and all is well now
                list(messages.get_messages(self.request))
                messages.error(self.request,
                               'Umebadilisha namba yako ya simu kwenda **' + new_phonenumber + '** kikamilifu')
            except Exception as e:
                list(messages.get_messages(self.request))
                messages.error(self.request,
                               'Ombi lako halijafanikiwa tafadhali wasiliana na uongozi kwa msaada zaidi')
            self.success_url = 'home'

        else:
            messages.error(self.request,
                           'Login kwanza ndipo utaweza kubadili namba yako ya simu')
            return redirect('loginPage')
        return super().form_valid(form)

class ChangePasswordView(FormView):
    template_name = "changepassword.html"
    form_class = ChangePasswordForm
    success_url = None

    def get(self, request):
        if request.user.is_authenticated:
            return super(FormView, self).get(request)
        else:
            messages.error(self.request, 'Login kwanza kwatumia password ya zamani ndo utaweza kubadilisha password yako')
            return redirect('loginPage')


    def form_valid(self, form):
        if self.request.user.check_password(form.cleaned_data['old_password']):
            self.request.user.set_password(form.cleaned_data['new_password'])
            self.request.user.save()
            user = authenticate(
                username = self.request.user.username,
                password=form.cleaned_data['new_password']
            )
            if user is not None:
                login(self.request, user)
            messages.success(self.request, 'Hongera umefanikiwa kubadili password yako')
            self.success_url = 'home'
        else:
            messages.error(self.request, 'Umekosea password ya zamani')
            self.success_url = 'changepassword'
        return super().form_valid(form)

class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = None

    def get(self, request):
        if request.user.is_authenticated:
            return redirect('Homepage')
        else:
            logout(request)
        return super(LoginView, self).get(request)

    def form_valid(self, form):
        user = authenticate(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password']
        )
        if user is not None:

            self.success_url = 'home'
            login(self.request, user)
            try:
                todaySales = Sales.objects.get(dealer=SysUser.objects.get(user=self.request.user,
                                                                          created_date__year=now().year,
                                                                          created_date__month=now().month,
                                                                          created_date__day=now().day))
                messages.success(self.request, "Mauzo ya leo ni dekoda :- " + str(todaySales.sold))
            except Exception as e:
                print(e)
        else:
            print('\n*********************************Loginpage**************************\n')
            self.success_url = 'login'


        return super().form_valid(form)

class DealerAssociateView(TemplateView):
    template_name = "home2.html"
    form_class = SalesForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DealerAssociateView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DealerAssociateView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['total_dealers'] = Dealer_ESP.objects.filter(
                dealerAssociate=SysUser.objects.get(user=self.request.user)).count()
            context['total_visits'] = DealerVisitData.objects.filter(
                dealer=Dealer_ESP.objects.filter(dealerAssociate = SysUser.objects.get(user=self.request.user))[0]
            ).count()
            return context
        else:
            redirect('loginPage')
            return context

class HomeView(FormView):
    template_name = "home.html"
    form_class = SalesForm
    success_url = None
    todaysales2 = None
    day, month, year = None, None, None

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(HomeView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['Date'] = now().today().strftime("%d/%m/%Y")
        try:
            self.todaysales2 = Sales.objects.get(dealer=SysUser.objects.get(user=self.request.user),
                                            created_date__year=now().year,
                                            created_date__month=now().month,
                                            created_date__day=now().day
                                            )
            messages.success(self.request,
                             'Mauzo yako ya leo ni dekoda: ' + str(self.todaysales2.sold))
            todaysmartcards = Smartcards.objects.filter(dealer=SysUser.objects.get(user=self.request.user)).filter(
                created_date__year=now().year,
                created_date__month=now().month,
                created_date__day=now().day
            )
            for smartcard in todaysmartcards:
                messages.success(self.request,
                                 'Smartcard :- ' + str(smartcard.smartcard_number))

            messages.success(self.request,
                             '*Unaweza badilisha idadi hapa chini*')
        except ObjectDoesNotExist:
            #No data entered today
            messages.success(self.request,
                             'Pick a date and enter the total number of decoders activated ')
        except Exception as e:
            print(e)
        return context

    def form_valid(self, form):
        print("Mosi HE")
        if self.request.user.is_authenticated:
            try:
                print("*********************"+form.cleaned_data['datepicker']+"*************************")
                #See if user has alreaady enter todays data
                splited_dates = form.cleaned_data['datepicker'].split('/')
                try:
                    self.month, self.day, self.year = int(splited_dates[0]), int(splited_dates[1]), int(splited_dates[2])
                except Exception as e:
                    print(e)
                    #Ikishidikana kusplit just use today
                    self.month, self.day, self.year = now().month, now().day, now().year
                todaysales = Sales.objects.filter(dealer=SysUser.objects.get(
                    user=User.objects.get(pk=self.request.user.id))).filter(
                    created_date__year=self.year,
                    created_date__month=self.month,
                    created_date__day=self.day
                )
                print('*************Duuh*****************')
                if todaysales:
                    messages.success(self.request,
                                     'You have already entered data for '+form.cleaned_data['datepicker'])

                    todaysales[0].sold = form.cleaned_data['sold']
                    todaysales[0].save()
                    self.request.method = 'GET'
                    self.success_url='smartcards'
                else:
                    #If not create an new object Hapa leo data bado haija ingizwa
                    #Created date ni siku ya mauzo
                    print('&&&&&&&&&'+form.cleaned_data['sold'])
                    todaysales = Sales(
                        sold=int(form.cleaned_data['sold']),
                        dealer=SysUser.objects.get(user=User.objects.get(pk=self.request.user.id)),
                        created_date=now(),
                        updated_date=now().date(),
                        sold_date=datetime.datetime(year=self.year, month=self.month, day=self.day)
                    )
                    print('&&&&&&&&&32323234')
                    todaysales.save()

                    self.request.method = 'GET'
                    self.success_url = 'smartcards'
            except Exception as e:
                print('\n******************6666666*******************\n')
                print(e)
                self.success_url = 'login'
            return super().form_valid(form)
        else:
            return redirect('loginPage')

class RegisterView(FormView):
    template_name = "register.html"
    form_class = RegisterForm
    success_url = 'megadealer'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        new_user = User.objects.create_user(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password'],
        )
        new_district = District(name=form.cleaned_data['district'], region=Region.objects.get(pk=form.cleaned_data['region'].id))
        new_district.save()
        SysUser(user=new_user,
                type=DealerType.objects.get(pk=form.cleaned_data['dealer_type'].id),
                region=Region.objects.get(pk=form.cleaned_data['region'].id),
                district=new_district,
                phoneNumber=form.cleaned_data['phoneNumber'],
                ).save()
        login(self.request, new_user)
        print('************************************'+form.cleaned_data['dealer_type'].name+'*************************')
        if form.cleaned_data['dealer_type'].name == 'Mega-dealer':
            self.success_url = 'home'

        return super().form_valid(form)

class SmartcardView(TemplateView):
    template_name = "smartcards.html"
    sold = None
    ipo_kwy_system = False
    format_sio_sahihi = False
    todaysales = None

    def get_context_data(self, **kwargs):
        context = super(SmartcardView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            todaysales = Sales.objects.filter(dealer=SysUser.objects.get(
                user=User.objects.get(pk=self.request.user.id))).order_by("-updated_date").first()
            self.sold = range(1, todaysales.sold+1)
            context['sold'] = self.sold  # you can add template variables!
            return context
        else:
            redirect('loginPage')
            return context

    def post(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            self.todaysales = Sales.objects.filter(dealer=SysUser.objects.get(
                user=User.objects.get(pk=self.request.user.id))).order_by("-updated_date").first()
            self.sold = range(1, self.todaysales.sold + 1)
            self.ipo_kwy_system = False
            #Checking if ipo kwenye system tyr
            for num in self.sold:
                not_mine = Smartcards.objects.filter(smartcard_number=self.request.POST.get(str(num))).exclude().exists()
                today_midnight = now().replace(hour=0, minute=0, second=1, microsecond=0)
                mine = Smartcards.objects.filter(smartcard_number=self.request.POST.get(str(num)),
                                               created_date__lt=today_midnight).exists()
                if mine & not_mine:
                    messages.error(self.request,
                                   'Smartcard : -' + self.request.POST.get(str(num)) + ' tayari ipo kwenye system')
                    self.ipo_kwy_system=True

            if self.ipo_kwy_system:
                return redirect('smartcards')
            else:
                print('\n******************Haipo*******************\n')
                #Checking if characters are 10 and can be converted to int
                for num in self.sold:
                    try:
                        int(self.request.POST.get(str(num)))
                        if len(self.request.POST.get(str(num))) != 10:
                            self.format_sio_sahihi = True
                            messages.error(self.request,
                                           'Smartcard : -' + self.request.POST.get(
                                               str(num)) + ' inatakiwa kuwa na tarakimu 10 tu')
                    except Exception as e:
                        self.format_sio_sahihi = True
                        messages.error(self.request,
                                       'Format ya smartcard : -' + self.request.POST.get(str(num)) + ' sio sahihi')

                #Checking the results of the upper test
                if self.format_sio_sahihi:
                    return redirect('smartcards')
                else:
                    #You can insert the new cards if the old card are okay

                    #First delete all the old smartcards
                    current_dealer = SysUser.objects.get(user=self.request.user)
                    todaysmartcards = Smartcards.objects.filter(dealer=current_dealer).filter(
                        created_date__year=now().year,
                        created_date__month=now().month,
                        created_date__day=now().day
                    )
                    todaysmartcards.delete()
                    #Nimeshafuta zilizopo sasa weka mpya
                    #messages.success(self.request,
                                 #    'Mauzo yako ni dekoda: ' + str(self.todaysales[0].sold))
                    for num in self.sold:
                        print(str(num))
                        print(self.request.POST.get(str(num)))
                        # no object satisfying query exists
                        smartcard_obj = Smartcards(
                            smartcard_number=self.request.POST.get(str(num)),
                            dealer=SysUser.objects.get(user=User.objects.get(pk=self.request.user.id)),
                            created_date=now()
                        )
                        smartcard_obj.save()
                        messages.success(self.request,
                                         'Smartcard number: - ' + self.request.POST.get(str(num)) + '')
            return render(self.request, 'thanks.html')
        else:
            return redirect('loginPage')

class MegaDealerView(FormView):
    template_name = 'megadealer.html'
    form_class = MegaDealerForm
    success_url = 'login'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MegaDealerView, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):

        kwargs = super(MegaDealerView, self).get_form_kwargs()
        kwargs.update({'region': SysUser.objects.get(user=User.objects.get(pk=self.request.user.id)).region})
        return kwargs

    def form_valid(self, form):
        if self.request.user.is_authenticated:
            esp = SysUser.objects.get(user=User.objects.get(pk=self.request.user.id))
            print('\n*********************************Homepage**************************\n')
            print(form.cleaned_data['megadealer'].id)
            megadealer= SysUser.objects.get(pk=form.cleaned_data['megadealer'].id)
            MegaDealer_Subdealer(esp=esp, megadealer=megadealer).save()
            self.success_url = 'home'
        return super().form_valid(form)

class RegisterDealerView(FormView):
    template_name = 'register_DA.html'
    form_class = RegisterDealerForm
    success_url = 'login'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(RegisterDealerView, self).dispatch(*args, **kwargs)

    @method_decorator(login_required)
    def form_valid(self, form):
        new_user = User.objects.create_user(
            username=form.cleaned_data['username'],
            password='pass12345*',
        )
        new_district = District(name=form.cleaned_data['district'],
                                region=Region.objects.get(pk=form.cleaned_data['region'].id))
        new_district.save()
        newSysUser = SysUser(user=new_user,
                type=DealerType.objects.get(pk=form.cleaned_data['dealer_type'].id),
                region=Region.objects.get(pk=form.cleaned_data['region'].id),
                district=new_district,
                phoneNumber=form.cleaned_data['phoneNumber'],
                )
        newSysUser.save()
        messages.success(self.request,
                         'Umefanikiwa kusajili ' + str(form.cleaned_data['username']))
        Dealer_ESP( dealer=newSysUser, dealerAssociate=SysUser.objects.get(user=self.request.user)).save()
        if form.cleaned_data['dealer_type'].name == 'Mega-dealer':
            self.success_url = 'home'

        return super().form_valid(form)

class DealerVisitView(FormView):
    template_name = 'DealerVisit.html'
    form_class = DealerVisitForm
    success_url = 'login'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DealerVisitView, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(DealerVisitView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    @method_decorator(login_required)
    def form_valid(self, form):
        dealerVisitData = DealerVisitData(
            dealer=form.cleaned_data['dealer'],
            StockAvailable = int(form.cleaned_data['StockAvailable']),
            StockPush = int(form.cleaned_data['StockPush']),
            StockBalance = int(form.cleaned_data['StockAvailable']) - int(form.cleaned_data['StockPush'])
        )
        if "mega" in form.cleaned_data['dealer'].dealer.type.name.lower():
            dealerVisitData.VisitType = 'Mega-Dealer'
        else:
            dealerVisitData.VisitType = 'Dealer Bypass'
        dealerVisitData.save()

        messages.success(self.request, 'Date submitted successfully')

        return super().form_valid(form)

class DealerActivationView(FormView):
    template_name = 'home.html'
    form_class = SalesForm
    success_url = 'login'


class MyReportsView(TemplateView):
    template_name = "myreport.html"