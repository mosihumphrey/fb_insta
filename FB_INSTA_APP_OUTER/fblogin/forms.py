from django import forms
from .models import User, DealerType, Region, SysUser,Dealer_ESP
from django.contrib.auth import authenticate


class RegisterForm(forms.Form):
    username = forms.CharField(label='', max_length=100, required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Username'}))
    password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Password'}), required=True,
                               min_length=4)
    password2 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Rudia password'}),
                                required=True, min_length=4)
    phoneNumber = forms.CharField(label='', max_length=100, required=True,
                              widget=forms.TextInput(attrs={'placeholder': 'Tigopesa namba mfano 0714123123'}))
    dealer_type = forms.ModelChoiceField(
        label='',
        queryset=DealerType.objects.all(),
        empty_label='Dealer wa aina gani...'
    )
    region = forms.ModelChoiceField(
        label='',
        queryset=Region.objects.all(),
        empty_label='Chagua mkoa..'
    )
    district = forms.CharField(label='', max_length=100, required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Wilaya:'}))

    def clean(self):
        cleaned_data = self.cleaned_data  # individual field's clean methods have already been called
        password1 = cleaned_data.get("password")
        password2 = cleaned_data.get("password2")

        if password1 != password2:
            self.add_error('password', 'Error -- password zinatakiwa kufanana')
            self.add_error('password2', 'Error -- password zinatakiwa kufanana')
            raise forms.ValidationError("Password zinatakiwa kufanana")

        return cleaned_data

    dealer_type.widget.attrs.update({'class': 'select-css'})
    region.widget.attrs.update({'class': 'select-css'})

    class Meta:
        model = User


class LoginForm(forms.Form):
    username = forms.CharField(label='', max_length=100, required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Username:'}))
    password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Password:'}), required=True)

    def clean(self):
        cleaned_data = self.cleaned_data
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')

        user = authenticate(
            username=username,
            password=password
        )
        if user is not None:
            pass
        else:
            self.add_error('username', 'Error -- wrong username or password')
            raise forms.ValidationError('Error -- wrong username or password')


class SalesForm(forms.Form):
    datepicker = forms.CharField(label='', required=True, max_length=10,
                           widget=forms.TextInput(attrs={'placeholder': 'Chagua tarehe'}))
    sold = forms.CharField(label='', required=True, max_length=10,
                               widget=forms.TextInput(attrs={'placeholder': 'Umeuza ngapi?'}))

class MegaDealerForm(forms.Form):

    def __init__(self, *args, **kwargs):
        region = kwargs.pop('region')
        super(MegaDealerForm, self).__init__(*args, **kwargs)
        self.fields['megadealer'].queryset = SysUser.objects.filter(type=DealerType.objects.get(name='Mega-dealer'),
                                                                    region=region)

    megadealer = forms.ModelChoiceField(
        label='',
        queryset=None,
        empty_label='Chagua Megadealer..'
    )
    megadealer.widget.attrs.update({'class': 'select-css'})


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Password ya zamani'}),
                                   required=True, min_length=4)
    new_password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Password mpya'}),
                                min_length=4)
    new_password2 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Rudia password mpya'}),
                                    min_length=4)

    def clean(self):
        cleaned_data = self.cleaned_data
        new_password = cleaned_data.get("new_password")
        new_password2 = cleaned_data.get("new_password2")

        if new_password2 != new_password:
            self.add_error('new_password', 'Error -- password zinatakiwa kufanana')
            self.add_error('new_password2', 'Error -- password zinatakiwa kufanana')
            raise forms.ValidationError("Password zinatakiwa kufanana")

        return cleaned_data

class ChangePhonenumberForm(forms.Form):
    phoneNumber = forms.CharField(label='', max_length=10, required=True, min_length=10,
                                  widget=forms.TextInput(attrs={'placeholder': 'Namba mpya ya Tigopesa'}))

    def clean(self):
        cleaned_data = self.cleaned_data
        phoneNumber = cleaned_data.get("phoneNumber")

        #Checking if a number start with the correct format
        if phoneNumber[:2] not in ['07', '06']:
            self.add_error('phoneNumber', 'Error -- Namba ya simu inatakiwa kuanzia na 07 au 06')
            raise forms.ValidationError("Namba ya simu inatakiwa kuanzia na 07 au 06")

        #Checking if the number has 10 character from the backend
        if len(phoneNumber) != 10:
            self.add_error('phoneNumber', 'Error -- Namba ya simu inatakiwa kuwa na tarakimu kumi tu mfano 0714123123')
            raise forms.ValidationError("Namba ya simu inatakiwa kuwa na tarakimu kumi tu mfano 0714123123")

        #Trying to convert the number into an int before submitting to DB
        try:
            int(phoneNumber)
        except ValueError:
            self.add_error('phoneNumber', 'Error -- Format ya namba ya simu inatakiwa kuwa hivi 0714123123')
            raise forms.ValidationError("Format ya namba ya simu inatakiwa kuwa hivi 0714123123")
        except Exception as e:
            self.add_error('phoneNumber', 'Error -- Tafadhali rudi tena kuweka namba ya simu')
            raise forms.ValidationError("Tafadhali rudi tena kuweka namba ya simu")

        #All validations are done
        return cleaned_data


class RegisterDealerForm(forms.Form):
    username = forms.CharField(label='', max_length=100, required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Jina'}))
    phoneNumber = forms.CharField(label='', max_length=100, required=True,
                                  widget=forms.TextInput(attrs={'placeholder': 'Weka namba mfano 0714123123'}))
    dealer_type = forms.ModelChoiceField(
        label='',
        queryset=DealerType.objects.exclude(name="Dealer Associate"),
        empty_label='Dealer wa aina gani...'
    )
    region = forms.ModelChoiceField(
        label='',
        queryset=Region.objects.all(),
        empty_label='Chagua mkoa..'
    )
    district = forms.CharField(label='', max_length=100, required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Wilaya:'}))

    def clean(self):

        try:
            e_user=User.objects.get(username=self.cleaned_data['username'])
            self.add_error('username', 'Error -- Dealer '+self.cleaned_data['username']+' ameshasajiliwa')
        except User.DoesNotExist:
            pass # tunatest mitambo
        return self.cleaned_data

    dealer_type.widget.attrs.update({'class': 'select-css'})
    region.widget.attrs.update({'class': 'select-css'})

    class Meta:
        model = User

class DealerVisitForm(forms.Form):


    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        print(self.user)
        super(DealerVisitForm, self).__init__(*args, **kwargs)
        self.fields['dealer'].queryset = Dealer_ESP.objects.filter(dealerAssociate = SysUser.objects.get(user=self.user))



    dealer = forms.ModelChoiceField(label='', queryset = None,
                                    empty_label='Chagua Dealer...'
                                    )
    StockAvailable = forms.CharField(label='', max_length=100, required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Stock available'}))
    StockPush = forms.CharField(label='', max_length=100, required=True,
                                     widget=forms.TextInput(attrs={'placeholder': 'Stock push'}))

    dealer.widget.attrs.update({'class': 'select-css'})

    def clean(self):
        cleaned_data = self.cleaned_data

        def check_int(data_2_check, form_field, error_message):
            try:
                int(data_2_check)
            except ValueError:
                self.add_error(form_field, error_message)


        check_int(cleaned_data.get("StockAvailable"), 'StockAvailable', 'Error -- Please enter a valid value')
        check_int(cleaned_data.get("StockPush"), 'StockPush', 'Error -- Please enter a valid value')
        try:
            if int(cleaned_data.get('StockAvailable')) > int(cleaned_data.get("StockPush")):
                pass

            else:
                self.add_error('StockPush', 'Error -- STOCK PUSH should be less than STOCK AVAILABLE')
        except:
            pass

        return self.cleaned_data
