from django.contrib import admin
from django.urls import path, include
from fblogin.views import LoginView, RegisterView, HomeView,SmartcardView, logoutView, MegaDealerView, \
    ChangePasswordView, ChangePhonenumberView, DealerAssociateView, RegisterDealerView, DealerVisitView,\
    DealerActivationView, MyReportsView
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),# Refresh token
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('login', LoginView.as_view(), name='loginPage'),
    path('register', RegisterView.as_view(), name='registerPage'),
    path('home', DealerAssociateView.as_view(), name='Homepage'),
    path('smartcards', SmartcardView.as_view(), name='smartcards'),
    path('logout', logoutView, name='logout'),
    path('megadealer', MegaDealerView.as_view(), name='megadealer'),
    path('changepassword', ChangePasswordView.as_view(), name='changephonenumber'),
    path('changephonenumber', ChangePhonenumberView.as_view(), name='megadealer'),
    path('DealerAssociate', DealerAssociateView.as_view(), name='DealerAssociate'),
    path('DealerVisit', DealerVisitView.as_view(), name='DealerVisit'),
    path('DealerActivation', HomeView.as_view(), name='DealerActivation'),
    path('registerDealer', RegisterDealerView.as_view(), name='registerDealer'),
    path('MyReports', MyReportsView.as_view(), name='MyReports'),
    path('', MegaDealerView.as_view(), name='logout'),



]
